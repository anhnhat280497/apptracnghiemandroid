package com.example.apptracnghiem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

public class Activity2 extends AppCompatActivity implements View.OnClickListener {

    Button btnLui, btnTien, btnResult;
    RadioButton radioDong, radioTay, radioNam, radioBac;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        setControll();
    }
    void setControll(){
        btnLui = (Button) findViewById(R.id.btnLui);
        btnTien = (Button) findViewById(R.id.btnTien);
        btnResult = (Button) findViewById(R.id.btnResult);
        radioDong = (RadioButton) findViewById(R.id.radioDong);
        radioTay = (RadioButton) findViewById(R.id.radioTay);
        radioNam = (RadioButton) findViewById(R.id.radioNam);
        radioBac = (RadioButton) findViewById(R.id.radioBac);

        btnLui.setText("<");
        btnTien.setText(">");

        btnLui.setOnClickListener(this);
        btnTien.setOnClickListener(this);
        btnResult.setOnClickListener(this);
        radioDong.setOnClickListener(this);
        radioTay.setOnClickListener(this);
        radioNam.setOnClickListener(this);
        radioBac.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.btnLui:
                intent = new Intent(this, Activity6.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break;
            case R.id.btnResult:
                intent = new Intent(this, Activity7.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break;

            case R.id.btnTien:
                intent = new Intent(this, Activity3.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break;
//            case R.id.radioDong:
//                radioBac.setChecked(false);
//                radioTay.setChecked(false);
//                radioNam.setChecked(false);
//                break;
//            case R.id.radioTay:
//                radioBac.setChecked(false);
//                radioDong.setChecked(false);
//                radioNam.setChecked(false);
//                break;
//            case R.id.radioNam:
//                radioBac.setChecked(false);
//                radioTay.setChecked(false);
//                radioDong.setChecked(false);
//                break;
//            case R.id.radioBac:
//                radioDong.setChecked(false);
//                radioTay.setChecked(false);
//                radioNam.setChecked(false);
//                break;
        }
    }
}
